#pragma once

#include "IGraph.h"
#include <set>

class SetGraph : public IGraph {
 public:
    SetGraph(int count) : graph_(count) {}
    SetGraph(const IGraph& graph);
    
    void AddEdge(int from, int to) override;
    int VerticesCount() const override;

    std::vector<int> GetNextVertices(int vertex) const override;
    std::vector<int> GetPrevVertices(int vertex) const override;

 private:
    std::vector<std::set<int>> graph_;
};