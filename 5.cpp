#include <vector>
#include <cassert>
#include <iostream>
#include <set>

/*
Дан неориентированный связный граф.
Требуется найти вес минимального остовного дерева в этом графе.
*/

class WeightedGraph {
 public:
    WeightedGraph(int count) : graph_(count) {}

    void putEdge(int from, int to, int weight) {
        assert(graph_.size() > from && graph_.size() > to && to >= 0 && from >= 0);
        graph_[from].push_back(std::make_pair(to, weight));
    }

    int getVertexesCount() const {
        return graph_.size();
    }

    std::vector<std::pair<int, int>> getEdges(int vertex) const {
        assert(graph_.size() > vertex && vertex >= 0);
        return graph_[vertex];
    }
    
 private:
    std::vector<std::vector<std::pair<int, int>>> graph_;
};

struct PairCompare {
    bool operator()(const std::pair<int, int>& lhs, const std::pair<int, int>& rhs) const{
        if (lhs.second < rhs.second) {
            return true;
        } else if (lhs.second == rhs.second && lhs.first < rhs.first) {
            return true;
        } else {
            return false;
        }
    }
};

int prim(const WeightedGraph& graph, int start) {
    std::vector<int> weight(graph.getVertexesCount(), INT32_MAX);
    std::set<std::pair<int, int>, PairCompare> queue;
    queue.insert(std::make_pair(start, 0));
    weight[start] = 0;
    while (!queue.empty()) {
        int vertex = (*queue.begin()).first;
        queue.erase(queue.begin());
        for (const auto& edge : graph.getEdges(vertex)) {
            if (weight[edge.first] == INT32_MAX) {
                weight[edge.first] = edge.second;
                queue.insert(edge);
            } else if (weight[edge.first] > edge.second && 
                    queue.count(std::make_pair(edge.first, weight[edge.first]))) {
                weight[edge.first] = edge.second;
                queue.erase(std::make_pair(edge.first, weight[edge.first]));
                queue.insert(edge);
            }
        }
    }

    int result = 0;
    for (int i = 0; i < graph.getVertexesCount(); i++) {
        result += weight[i];
    }

    return result;
}

int main() {
    int vertex_count = 0;
    int edge_count = 0;
    std::cin >> vertex_count >> edge_count;
    WeightedGraph graph(vertex_count);
    int from = 0;
    int to = 0;
    int weight = 0;
    for (int i = 0; i < edge_count; i++) {
        std::cin >> from >> to >> weight;
        graph.putEdge(from - 1, to - 1, weight);
        graph.putEdge(to - 1, from - 1, weight);
    }

    std::cout << prim(graph, 0) << std::endl;
}