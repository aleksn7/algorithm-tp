#include <vector>
#include <cassert>
#include <iostream>
#include <set>

#define DOES_NOT_EXIST -1

/*
Требуется отыскать самый короткий маршрут между городами.
Из города может выходить дорога, которая возвращается в этот же город.
Требуемое время работы O((N + M)log N), где N – количество городов, M – известных дорог между ними.
N ≤ 10000, M ≤ 250000.
Длина каждой дороги ≤ 10000.
*/

class WeightedGraph {
 public:
    WeightedGraph(int count) : graph_(count) {}

    void putEdge(int from, int to, int weight) {
        assert(graph_.size() > from && graph_.size() > to && to >= 0 && from >= 0);
        graph_[from].push_back(std::make_pair(to, weight));
    }

    int getVertexesCount() const {
        return graph_.size();
    }

    std::vector<std::pair<int, int>> getEdges(int vertex) const {
        assert(graph_.size() > vertex && vertex >= 0);
        return graph_[vertex];
    }
    
 private:
    std::vector<std::vector<std::pair<int, int>>> graph_;
};

struct PairCompare {
    bool operator()(const std::pair<int, int>& lhs, const std::pair<int, int>& rhs) const{
        if (lhs.second < rhs.second) {
            return true;
        } else if (lhs.second == rhs.second && lhs.first < rhs.first) {
            return true;
        } else {
            return false;
        }
    }
};

int dijkstra(const WeightedGraph& graph, int start, int stop) {
    std::vector<int> path_length(graph.getVertexesCount(), INT32_MAX);
    std::set<std::pair<int, int>, PairCompare> queue;
    queue.insert(std::make_pair(start, 0));
    path_length[start] = 0;
    while (!queue.empty()) {
        int vertex = (*queue.begin()).first;
        if (vertex == stop) {
            return path_length[stop];
        }
        queue.erase(queue.begin());
        for (const auto& edge : graph.getEdges(vertex)) {
            if (path_length[edge.first] == INT32_MAX) {
                path_length[edge.first] = path_length[vertex] + edge.second;
                queue.insert(std::make_pair(edge.first, path_length[edge.first]));
            } else if (path_length[edge.first] > path_length[vertex] + edge.second) {
                path_length[edge.first] = path_length[vertex] + edge.second;
                queue.erase(std::make_pair(edge.first, edge.second));
                queue.insert(std::make_pair(edge.first, path_length[edge.first]));
            }
        }
    }

    return DOES_NOT_EXIST;
}

int main() {
    int vertex_count = 0;
    int edge_count = 0;
    std::cin >> vertex_count >> edge_count;
    WeightedGraph graph(vertex_count);
    int from = 0;
    int to = 0;
    int weight = 0;
    for (int i = 0; i < edge_count; i++) {
        std::cin >> from >> to >> weight;
        graph.putEdge(from, to, weight);
        graph.putEdge(to, from, weight);
    }

    std::cin >> from >> to;
    std::cout << dijkstra(graph, from, to) << std::endl;
}