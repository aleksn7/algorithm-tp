#include <iostream>
#include <array>
#include <cassert>
#include <set>
#include <cstring>
#include <algorithm>
#include <unordered_map>

/*
Написать алгоритм для решения игры в “пятнашки”.
Решением задачи является приведение к виду: 
[ 1 2 3 4 ] [ 5 6 7 8 ] [ 9 10 11 12] [ 13 14 15 0 ], 
где 0 задает пустую ячейку. Достаточно найти хотя бы какое-то решение. 
Число перемещений костяшек не обязано быть минимальным.
*/

#define SIDE_SIZE 4
#define FIELD_SIZE (SIDE_SIZE * SIDE_SIZE)

std::array<int, FIELD_SIZE> finish_field({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0});

class GameState {
 public:
    GameState(const std::array<int, FIELD_SIZE>& field) : field_(field) {
        for (int i = 0; i < FIELD_SIZE; i++) {
            if (field_[i] == 0) {
                zero_pos_ = i;
                break;
            }
        }
    }

    bool canMoveRight() const {
        return zero_pos_ % SIDE_SIZE;
    }

    bool canMoveLeft() const {
        return zero_pos_ % SIDE_SIZE < SIDE_SIZE - 1;
    }

    bool canMoveUp() const {
        return zero_pos_ < FIELD_SIZE - SIDE_SIZE;
    }

    bool canMoveDown() const {
        return zero_pos_ > SIDE_SIZE - 1;
    }

    GameState moveUp() const {
        assert(canMoveUp());
        GameState new_state(*this);
        std::swap(new_state.field_[zero_pos_], new_state.field_[zero_pos_ + SIDE_SIZE]);
        new_state.zero_pos_ += SIDE_SIZE;
        return new_state;
    }

    GameState moveDown() const {
        assert(canMoveDown());
        GameState new_state(*this);
        std::swap(new_state.field_[zero_pos_], new_state.field_[zero_pos_ - SIDE_SIZE]);
        new_state.zero_pos_ -= SIDE_SIZE;
        return new_state;
    }

    GameState moveRight() const {
        assert(canMoveRight());
        GameState new_state(*this);
        std::swap(new_state.field_[zero_pos_], new_state.field_[zero_pos_ - 1]);
        new_state.zero_pos_--;
        return new_state;

    }

    GameState moveLeft() const {
        assert(canMoveLeft());
        GameState new_state(*this);
        std::swap(new_state.field_[zero_pos_], new_state.field_[zero_pos_ + 1]);
        new_state.zero_pos_++;
        return new_state;
    }

    bool operator==(const GameState& rhs) const {
        return field_ == rhs.field_;
    }

    bool operator<(const GameState& rhs) const {
        return field_ < rhs.field_;
    }

    bool isFinish() const {
        return field_ == finish_field;
    }

    int getHeuristic() const {
        int result = 0;
        for (int i = 0; i < FIELD_SIZE; i++) {
            if (field_[i] == 0) {
                continue;
            }

            int x = i % SIDE_SIZE;
            int y = i / SIDE_SIZE;

            int x_ = (field_[i] - 1) % SIDE_SIZE;
            int y_ = (field_[i] - 1) / SIDE_SIZE;

            result += abs(x - x_) + abs(y - y_);
        }
        return result;
    }

    friend std::ostream& operator<<(std::ostream& stream, const GameState& state) {
        for (int i = 0; i < FIELD_SIZE; i++) {
            if (!(i % SIDE_SIZE)) {
                stream << std::endl;
            }
            stream << state.field_[i] << " ";
        }
        return stream;
    }

    friend struct Hash;

 private:
    std::array<int, FIELD_SIZE> field_;
    int zero_pos_;
};

struct Hash {
	size_t operator()(const GameState& state) const {
		size_t hash = 0;
        for (int i = 0; i < FIELD_SIZE; i++) {
            if (state.field_[i] == 0) {
                continue;
            }
            hash += abs(state.field_[i] - i - 1);
        }
		return hash;
	}
};

std::pair<int, std::string> getSolution(const GameState& state) {
    std::set<std::pair<int, GameState>> queue;
    queue.insert(std::make_pair(0, state));

    std::unordered_map<GameState, char, Hash> visited;
	visited[state] = 'S';

    bool flag = false;
    while (!queue.empty()) {
        GameState new_state = (*queue.begin()).second;
        if (new_state.isFinish()) {
            flag = true;
            break;
        }
        queue.erase(queue.begin());
        if (new_state.canMoveLeft()) {
            GameState tmp_state = new_state.moveLeft();
            if (!visited.count(tmp_state)) {
                visited[tmp_state] = 'L';
                queue.insert(std::make_pair(tmp_state.getHeuristic(), tmp_state));
            }
        }
        if (new_state.canMoveRight()) {
            GameState tmp_state = new_state.moveRight();
            if (!visited.count(tmp_state)) {
                visited[tmp_state] = 'R';
                queue.insert(std::make_pair(tmp_state.getHeuristic(), tmp_state));
            }
        }
        if (new_state.canMoveUp()) {
            GameState tmp_state = new_state.moveUp();
            if (!visited.count(tmp_state)) {
                visited[tmp_state] = 'U';
                queue.insert(std::make_pair(tmp_state.getHeuristic(), tmp_state));
            }
        }
        if (new_state.canMoveDown()) {
            GameState tmp_state = new_state.moveDown();
            if (!visited.count(tmp_state)) {
                visited[tmp_state] = 'D';
                queue.insert(std::make_pair(tmp_state.getHeuristic(), tmp_state));
            }
        }
    }

    if (!flag) {
        return std::make_pair(-1, std::string());
    }

    std::string result;
	GameState temp_state(finish_field);
	char move = visited[temp_state];
    int i = 0;
	while (move != 'S') {
        i++;
		result += move;
		switch (move) {
		case 'L':
			temp_state =  temp_state.moveRight();
			break;
		case 'U':
			temp_state =  temp_state.moveDown();
			break;
		case 'R':
			temp_state =  temp_state.moveLeft();
			break;
		case 'D':
			temp_state =  temp_state.moveUp();
			break;
		}
		move = visited[ temp_state];
	}

	std::reverse(result.begin(), result.end());
	return std::make_pair(i, result);
}

int main() {
    std::array<int, FIELD_SIZE> arr;
    for (int i = 0; i < FIELD_SIZE; i++) {
        std::cin >> arr[i];
    }
    auto solution = getSolution(GameState(arr));
    std::cout << solution.first << std::endl << solution.second << std::endl;
}