#include "MatrixGraph.h"
#include <cassert>

MatrixGraph::MatrixGraph(const IGraph& graph) : 
        graph_(graph.VerticesCount(), std::vector<bool>(graph.VerticesCount(), true)) {
    for (int i = 0; i < graph.VerticesCount(); i++) {
        for (const auto& edge : graph.GetNextVertices(i)) {
            graph_[i][edge] = true;
        }
    }
}
    
void MatrixGraph::AddEdge(int from, int to) {
    assert(graph_.size() > from && graph_.size() > to && to >= 0 && from >= 0);
    graph_[from][to] = true;
}

int MatrixGraph::VerticesCount() const {
    return graph_.size();
}

std::vector<int> MatrixGraph::GetNextVertices(int vertex) const {
    assert(graph_.size() > vertex && vertex >= 0);
    std::vector<int> return_vec;
    for (int i = 0; i < graph_.size(); i++) {
        if (graph_[vertex][i]) {
            return_vec.push_back(i);
        }
    }

    return return_vec;
}

std::vector<int> MatrixGraph::GetPrevVertices(int vertex) const {
    assert(graph_.size() > vertex && vertex >= 0);
    std::vector<int> return_vec;
    for (int i = 0; i < graph_.size(); i++) {
        if (graph_[i][vertex]) {
            return_vec.push_back(i);
        }
    }

    return return_vec; 
}