#include "SetGraph.h"
#include <cassert>

SetGraph::SetGraph(const IGraph& graph) : graph_(graph.VerticesCount()) {
    for (int i = 0; i < graph.VerticesCount(); i++) {
        for (const auto& edge : graph.GetNextVertices(i)) {
            graph_[i].insert(edge);
        }
    }
}
    
void SetGraph::AddEdge(int from, int to) {
    assert(graph_.size() > from && graph_.size() > to && to >= 0 && from >= 0);
    graph_[from].insert(to);
}

int SetGraph::VerticesCount() const {
    return graph_.size();
}

std::vector<int> SetGraph::GetNextVertices(int vertex) const {
    assert(graph_.size() > vertex && vertex >= 0);
    std::vector<int> return_vec;
    for (const auto& edge : graph_[vertex]) {
        return_vec.push_back(edge);
    }

    return return_vec;
}

std::vector<int> SetGraph::GetPrevVertices(int vertex) const {
    assert(graph_.size() > vertex && vertex >= 0);
    std::vector<int> return_vec;
    for (int i = 0; i < graph_.size(); i++) {
        if (graph_[i].count(vertex)) {
            return_vec.push_back(i);
        }
    }

    return return_vec;
}