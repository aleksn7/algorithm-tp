#include <iostream>
#include <cassert>
#include <vector>
#include <queue>
#include <set>

/*
Дан невзвешенный неориентированный граф. В графе может быть несколько 
кратчайших путей между какими-то вершинами. Найдите количество различных 
кратчайших путей между заданными вершинами. Требуемая сложность O(V+E).
*/

class IGraph {
 public:
    virtual ~IGraph() {}

    virtual void AddEdge(int from, int to) = 0;

	virtual int VerticesCount() const  = 0;

    virtual std::vector<int> GetNextVertices(int vertex) const = 0;
    virtual std::vector<int> GetPrevVertices(int vertex) const = 0;
};

class ListGraph : public IGraph {
 public:
    ListGraph(int count) : graph_(count) {}
    ListGraph(const IGraph& graph);
    
    void AddEdge(int from, int to) override;
    int VerticesCount() const override;

    std::vector<int> GetNextVertices(int vertex) const override;
    std::vector<int> GetPrevVertices(int vertex) const override;

 private:
    std::vector<std::vector<int>> graph_;
};

ListGraph::ListGraph(const IGraph& graph) {
    for (int i = 0; i < graph.VerticesCount(); i++) {
        graph_[i] = graph.GetNextVertices(i);
    }
}

void ListGraph::AddEdge(int from, int to) {
    assert(graph_.size() > from && graph_.size() > to && from >= 0 && to >= 0);
    graph_[from].push_back(to);
}

int ListGraph::VerticesCount() const {
    return graph_.size();
}

std::vector<int> ListGraph::GetNextVertices(int vertex) const {
    assert(graph_.size() > vertex && vertex >= 0);
    return graph_[vertex];
}

std::vector<int> ListGraph::GetPrevVertices(int vertex) const {
    assert(graph_.size() > vertex && vertex >= 0);
    std::vector<int> return_vec;
    for (int i = 0; i < graph_.size(); i++) {
        for (const auto& edge : graph_[i]) {
            if (edge == vertex) {
                return_vec.push_back(i);
                break;
            }
        }
    }

    return return_vec;
}

int getMinPathCount(const IGraph& graph, int start, int stop) {
    std::vector<int> path_length(graph.VerticesCount(), INT32_MAX);
    std::vector<int> path_count(graph.VerticesCount(), 0);
    std::queue<int> q;
    q.push(start);
    path_length[start] = 0;
    path_count[start] = 1;
    while (!q.empty()) {
        int vertes = q.front();
        q.pop();
        for (const auto& edge : graph.GetNextVertices(vertes)) {
            if (path_length[edge] > path_length[vertes] + 1) {
                path_length[edge] = path_length[vertes] + 1;
                path_count[edge] = path_count[vertes];
                q.push(edge);
            } else if (path_length[edge] == path_length[vertes] + 1) {
                path_count[edge] += path_count[vertes];
            }
        }
    }

    return path_count[stop];
}

int main() {
    int edge_count = 0;
    int vertex_count = 0;
    std::cin >> vertex_count >> edge_count;
    ListGraph graph(vertex_count);
    int edge, vertex;
    for (int i = 0; i < edge_count; i++) {
        std::cin >> vertex >> edge;
        graph.AddEdge(vertex, edge);
        graph.AddEdge(edge, vertex);
    }
    std::cin >> vertex >> edge;

    std::cout << getMinPathCount(graph, vertex, edge) << std::endl;
}