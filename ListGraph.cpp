#include "ListGraph.h"
#include <cassert>

ListGraph::ListGraph(const IGraph& graph) {
    for (int i = 0; i < graph.VerticesCount(); i++) {
        graph_[i] = graph.GetNextVertices(i);
    }
}

void ListGraph::AddEdge(int from, int to) {
    assert(graph_.size() > from && graph_.size() > to && to >= 0 && from >= 0);
    graph_[from].push_back(to);
}

int ListGraph::VerticesCount() const {
    return graph_.size();
}

std::vector<int> ListGraph::GetNextVertices(int vertex) const {
    assert(graph_.size() > vertex && vertex >= 0);
    return graph_[vertex];
}

std::vector<int> ListGraph::GetPrevVertices(int vertex) const {
    assert(graph_.size() > vertex && vertex >= 0);
    std::vector<int> return_vec;
    for (int i = 0; i < graph_.size(); i++) {
        for (const auto& edge : graph_[i]) {
            if (edge == vertex) {
                return_vec.push_back(i);
                break;
            }
        }
    }

    return return_vec;
}
