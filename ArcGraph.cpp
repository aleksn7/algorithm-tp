#include "ArcGraph.h"
#include <cassert>

ArcGraph::ArcGraph(const IGraph& graph) {
    for (int i = 0; i < graph.VerticesCount(); i++) {
        for (const auto& edge : graph.GetNextVertices(i)) {
            graph_.push_back(std::make_pair(i, edge));
        }
    }
}
    
void ArcGraph::AddEdge(int from, int to) {
    assert(vertexes_ > from && vertexes_ > to && from >= 0 && to >= 0);
    graph_.push_back(std::make_pair(from, to));
}

int ArcGraph::VerticesCount() const {
    return graph_.size();
}

std::vector<int> ArcGraph::GetNextVertices(int vertex) const {
    assert(vertexes_ > vertex && vertex >= 0);

    std::vector<int> return_vec;
    for (const auto& vert : graph_) {
        if (vert.first == vertex) {
            return_vec.push_back(vert.second);
        }
    }

    return return_vec;
}

std::vector<int> ArcGraph::GetPrevVertices(int vertex) const {
    assert(vertexes_ > vertex && vertex >= 0);

    std::vector<int> return_vec;
    for (const auto& vert : graph_) {
        if (vert.second == vertex) {
            return_vec.push_back(vert.first);
        }
    }

    return return_vec;
}